/**
 * @file
 * Drupal Javascript behaviors for Webform Fetchify Address field.
 */

(function ($, Drupal) {
  Drupal.behaviors.webformCraftyclicks = {
    attach: function (context) {
      $('.webform-address-craftyclicks--wrapper', context).each(function () {
        var $form = $(this).closest('.webform-submission-form');
        var $postcodeLookupButton = $(this).find(
          '.crafty-clicks-postcode-lookup-button'
        );
        var ccPostcodeLookupObject = CraftyPostcodeCreate();

        ccPostcodeLookupObject.set(
          'access_token',
          $('input[name="crafty_token"]', $form).val()
        );
        ccPostcodeLookupObject.set(
          'result_elem_id',
          $('#crafty_postcode_result_display', $form).attr('id')
        );
        ccPostcodeLookupObject.set('form', $($form).attr('id'));
        ccPostcodeLookupObject.set(
          'elem_company',
          $('input[name*="[company]"]', $form).attr('name')
        );
        ccPostcodeLookupObject.set(
          'elem_street1',
          $('input[name*="[address1]"]', $form).attr('name')
        );
        ccPostcodeLookupObject.set(
          'elem_street2',
          $('input[name*="[address2]"]', $form).attr('name')
        );
        ccPostcodeLookupObject.set(
          'elem_street3',
          $('input[name*="[address3]"]', $form).attr('name')
        );
        ccPostcodeLookupObject.set(
          'elem_town',
          $('input[name*="[town]"]', $form).attr('name')
        );
        ccPostcodeLookupObject.set(
          'elem_postcode',
          $('input[name*="[postcode]"]', $form).attr('name')
        );
        ccPostcodeLookupObject.set('town_uppercase', 0);
        ccPostcodeLookupObject.set('res_autoselect', 0);
        ccPostcodeLookupObject.set(
          'busy_img_url',
          '/core/misc/throbber-active.gif'
        );

        $postcodeLookupButton.on('click', function lookup() {
          ccPostcodeLookupObject.doLookup();
        });
      });
    }
  }
})(jQuery, Drupal);
