## Webform Crafty Clicks

This module provides a Webform Element plugin for Webforms to add
[Fetchify UK postcode lookup][cc] to the Webform Address composite element.

[cc]: https://fetchify.com/data-intelligence-tools/uk-postcode-lookup-royal-mail-paf/

### Setup

This module is dependent on [Webform][webform].

You will need to obtain an access token from Crafty Clicks.
Please add it as a config setting to your `settings.php` file (or equivalent).

```PHP
$config['webform_craftyclicks.settings']['access_token'] = 'xxxxx-xxxxx-xxxxx-xxxxx';
```

[webform]: https://www.drupal.org/project/webform

### Supporting organizations:
[Christian Aid][ca]

[ca]: https://www.christianaid.org.uk/